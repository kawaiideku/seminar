---
documentclass:   report
numbersections:  yes
geometry:
- top=25mm
- left=30mm
- right=30mm
header-includes: |
    \usepackage{fancyhdr}
    \usepackage{graphicx}

    \graphicspath{ {./ppt-slides/} }

    \pagestyle{fancy}
    \fancyhf{}
    \fancyhead[R]{\textit{Architecture-Aware Approximate Computing}}
    \fancyfoot[L]{
	    \textit{Dept. of CSE, RIT Kottayam}
    }
    \fancyfoot[R]{
	    \thepage
    }
    \setlength{\headheight}{1pt}
    \renewcommand{\headrulewidth}{0.4pt}
    \renewcommand{\footrulewidth}{0.4pt}

indent:          12.5mm
linestretch:     1.5
papersize:       a4
mainfont:        Times New Roman
fontsize:        12pt

---

\pagenumbering{roman}
\addtocontents{toc}{\textbf{Abstract}~\hfill\textbf{ii}\par}

\setcounter{page}{3}
\tableofcontents

\cleardoublepage
\addcontentsline{toc}{chapter}{\listfigurename}
\listoffigures

\newpage
\pagenumbering{arabic}

# \large INTRODUCTION
Constraints imposed by hardware scaling and data and control dependencies
across computations combined by compilers' inability to maximize computation
parallelism limit the performance potential of multithreaded workloads running
on emerging manycore systems. State-of-the-art research on computer
architecture , optimizing/parallelizing compilers and runtime systems/OS  helps
us extract increasingly more performance from modern architectures, but their
impact is hampered by ever-growing application and hardware complexities. In
particular, data accesses in modern architectures pose a significant bottleneck
(from both performance and energy consumption angles) as they go through multiple layers
in hardware, each with its own management strategy. Indeed, there is a growing
concern that memory wall can be the main factor preventing many
important applications from achieving their full potential, even if they employ
sophisticated code parallelization and data optimization techniques.  Thus,
there is a motivation for exploring revolutionary approaches instead of
evolutionary ones.  any workloads in different application domains can live
with a less-than-perfect output quality. The application programmers are
usually provided with metrics to evaluate the output quality of a particular
application. For instance, in video encoding/decoding applications, Peak Signal
to Noise Ratio (PSNR) is a metric that is widely used to measure the quality of
lossy video compression. Observing this, performance and energy benefits that arise from
deliberate use of so-called approximate computing has recently been explored
across software and hardware stacks. Approximate
computing can be a promising solution in overcoming the inherent performance
and energy scalability issues faced by current parallel hardware and software
systems due to data accesses. For example, skipping some computations can save
both computation cycles/energy and cache/memory cycles/energy, which could not
be achieved by employing conventional code/data restructuring or hardware
enhancements alone. In fact, dropping computations/data accesses can be one of
the ways to take the performance/energy scalability of parallel workloads to
levels that are not possible through evolutionary approaches.

# \large  TARGET MANYCORE ARCHITECTURE

![Representation of Network-on-Chip](images/noc.png)

Figure 1 shows the architecture of a state-of-the-art network-on-chip (NoC)
based manycore system (similar to Intel KNL  and many emerging commercial
manycore systems) and the low of a data access in it under the SNUCA last-level
cache (LLC) management policy 1. First, the local L1 cache is looked up ( 1 ),
and if a miss occurs, an L2 bank (which is determined based on the physical
address of the data being requested) is accessed ( 2 ). If they hit in the target
L2 bank, the data is read and transferred to the local L1 of the requesting
core ( 6 ). If not, an of-chip memory access needs to be performed. For this,
first, a target memory controller (C) is determined (again, based on the
physical address of the data being requested) and they access the target bank
governed by that controller via the on-chip network ( 3 ). Each bank is
equipped with a row-buffer that holds the most recently accessed memory row
(also called page) typically, a row-buffer hit takes much less time than a
row-buffer miss, as the latter requires an access to the memory array itself.


# \large  DROPPING DATA ACCESSES

While there exist different approaches  to approximate computing, each
leading to a different trade-off between performance/energy benefits and program
output accuracy, in this work they use skipping/dropping data accesses.

They want to emphasize that, while dropping data accesses can lead to program
crash in certain cases, this is not expected to be the case in their target
application domain. Basically, in this work, they focus on array and loop
dominated programs from high performance computing and embedded image/video
processing. These codes are generally written as a series of loop nests, and
each nest having a large body of instructions. The specific execution path taken
by the program is mostly a function of the input size but is not dependent on
the actual values of the inputs. Thus, changes in the values of the data
elements (which is an effect of their approach, as they supply a value for each
data access they drop) does not cause an otherwise correct program to crash.

The goal behind data access dropping/skipping is to drop the right number of
data accesses to maximize the performance/energy benefits and at the same time
remain within the limits of acceptable output inaccuracy (output quality).
Clearly, the latter is a function of the application/workload characteristics
and can sometimes even change from one execution environment (e.g., user
constraints, input) to another, for the same application program.

# \large  BENCHMARKS AND EVALUATION

The program slicing technique (and the required code analysis) used in this
work is implemented within the LLV framework. Most of their experiments are
conducted using a cycle-accurate manycore simulator. More specifically, in their
experiments, they use the GE5cPAT [3, 20] combination to collect performance
and energy statistics. Note that the reported energy numbers include the energy
consumed by all CPU components, caches, TLBs as well as main memory.

![Data locality for different workloads](images/locality.png)

In this study, they used 10 multithreaded benchmark programs 3. In their
experiments, only one multithreaded application is executed at a time (using
all 32 cores). In this column, Relative Difference is the root-mean-square
error of the output. Note that, the same error metrics have also been used by
prior research on approximate computing. For x264 and ImgSmooth, they used
Peak Signal to Noise Ratio (PSNR) as error metric to measure the quality of
output images. For Ferret, they used classification accuracy (calculated by the
average of similarity differences). It can be observed from this column that their
workloads employ a variety of error (quality) metrics. Finally, the last column
of this table gives the total amount of input dataset in Bs. They start by
presenting the breakdown of data accesses for each benchmark program they have
into the seven locality categories (C~1~...C~7~) explained earlier in
Section 2. The results plotted in Figure indicate that their applications
exhibit great variety in terms of the data locality they exhibit. For example,
locality behaviors of ImgSmooth and SSI are quite well, with very few data
accesses fall in the C~4~ - C~7~ range. In comparison, applications such as
Barnes-Hut and x264 exhibit poor data locality, with nearly most of their data
accesses falling in the C~4~ - C~7~ range.


## \normalsize  BENEFITS AND OUTPUT INACCURACY TRADE-OFF

They evaluate their benchmark programs in terms of their different metrics while
varying the fraction of data accesses to be dropped memory access latency
improvement, overall performance improvement, energy savings, and output error
(inaccuracy). All these metrics are given as relative (percentage) values with
respect to the default execution where no data access is dropped (in this
default case, the program output quality is assumed to be perfect). Recall that
the third column of Table 2 gives the error (quality) metric for each
benchmark. In the x axes of the graphs presented below, 5% of C7 refers to an
execution where 5% of the data accesses in the C 7 category (as defined in
Section 2) are dropped. Similarly, 100% of C7  5% of C6 captures an
execution where all of the C 7 type of data accesses as well as 5% of the C 6
type of data accesses are dropped, and so on. Note that the percentage
performance (energy) improvement values represent the percentage
reduction/saving over the execution time (energy consumption) of the default
execution. Also, each result presented below in Figures 4, 5, 6, and 7
represents the median values collected from 20 different executions. Finally,
in an execution where they are to drop a certain fraction of data accesses, those
accesses are determined randomly. For example, if, say, they are dropping 10% of
the C 7 type of data accesses, that 10% accesses are selected randomly (from
among all C 7 type of accesses).

![Latency and Performance Improvements](images/latPerfImp.png)

![Energy and Performance Improvements](images/energyPerfImp.png)

The graph in Figure 4 gives the percentage improvement in the average data
access latency under different execution scenarios (the x-axis represents the
fraction and category of the data accesses dropped). As expected, data access
latencies drop, as they increase the number of data accesses to drop. However,
improvement trends vary across different workloads. For example, SSI does not
exhibit significant improvements except in the last two executions. In contrast,
LU and VolRend show reasonable improvements in memory access latencies, even
when the number of data accesses dropped is not too high. While data access
latency is certainly an important metric, one would ultimately be interested in
overall application performance, which is plotted in Figure 5. This graph gives
the percentage reduction in the parallel execution time (over the default
execution).  Clearly, different applications get different benefits due to the
different contributions of the memory accesses in them to their overall
performance. Still, one can observe a common pattern where the performance
benefits are initially not very high but they show a significant jump beyond a
certain point. For example, in VolRend, the performance benefits jump
significantly as they move from 50% of C 7  to 100% of C 7. A similar jump
can be observed in x264 and Barnes-Hut as well.  Figure 6 plots the energy
benefits (given as percentage reduction in system-wide energy consumption over
the default execution) when dropping various number of data accesses (as
specified on the x-axis). They see that these results are in general higher than
the performance improvement results presented in Figure 5. This is primarily
because, in the default execution, a costly data access can overlap with other
(ongoing) data accesses or computations. Consequently, dropping such an access
may not return much benefits in practice. In contrast, while performance
overhead of a data access can be hidden in parallel execution, its energy
overhead cannot be and, as a result, dropping a data access gives more energy
benefits than performance benefits, when compared to the default execution.
Overall, the results presented in Figures 5 and 6 clearly indicate that
dropping costly memory accesses can give significant performance and energy
benefits. They see that architecture aware performs better than random skipping.

![Architecture-Aware vs Random Skipping](images/4.png)

![Error values](images/5.png)

# \large  PROGRAM SLICING-BASED DATA ACCESS REMOVAL

## \normalsize PROGRAM SLICING BASICS

Program slicing is a stheirce code analysis strategy that identifies code segments
relevant to a given program point in the target code base. The initial idea was
proposed by Weiser for the purpose of simplifying debugging. Slicing is
also used in program differencing, optimization, maintenance, and low back
analysis. While the original concept ensures that a code slice is a
syntactically valid program, more recent algorithms relax this constraint to
provide more accurate (smaller) slices. There are two main forms of program
slicing. Static slicing, which is the one employed in this work, uses only
statically measurable information and it is applicable for any execution of the
program. On the other hand, dynamic slicing is tailored to a particular
execution of the program which requires a fixed set of input values.

![Performance Improvements](images/7.png)

A program slice can be expressed as a set of data and control dependencies.
There are two "directions" a program slice can be extracted since a dependency
requires two actors. Backward Slice of a given point is the union of the points
it depends on. Similarly, Forward Slice of a point is the union of its
dependent points. Since both of these are calculated are using the same
dependency relations, their explanation below mostly focuses on backward slicing.

## \large  EXPERIMENTAL EVALUATION

To demonstrate the effectiveness of their proposed slicing based approach to
approximate computing, they give in Figure 12 plots showing how much performance
improvement their approach and random selection of the data accesses bring under
the same error bound. The error bound in these experiments takes values from
{1%, 2%, 4%, 6%, 8%, 10%}. In obtaining the results captured by the first bar
for each application, they used the graph in Figure 5 discussed earlier. More
specifically, for each error bound tested, they identified the maximum performance
savings possible. The second bar in each plot in Figure 12 represents the
results from their slicing based approach under the same error bound. This
iso-error analysis clearly indicates that, instead of randomly dropping the
costly data references, employing a slicing based approach can save
significantly more performance, the geometric means of the respective
improvements with the random strategy and slicing based approach being 3.6% and
8.8%, respectively, when the error bound is set to 2%. The corresponding
performance improvements are 12.9% and 21.5%, respectively, when the error
bound is set to 6%.  The corresponding energy savings under the same error
bounds are plotted in Figure 14 (the first bar for each application is derived
from Figure 6). It can be seen that, the general trends in energy savings are
similar to those in performance improvements. Specifically, when the error bound
is set to 2%, randomly dropping the costly data accesses and their slicing based scheme generate 5.6% and 13.6% energy savings (geometric means). The improvements jump to 20% and 33.2%, respectively, when the acceptable error bound is increased to 6%.


# \large  RELATED WORK

**Hardware Support**: Hedge et al. explored low-energy digital signal processing
as well as algorithmic noise tolerance schemes. Venkataramani et al.
developed Quara, a quality-programmable vector processor where the ISA provides
accuracy levels for instructions. ohapatra et al.  investigated voltage
over-scaling (VOD) and proposed two techniques, dynamic segmentation and delay
budgeting, to improve the scalability of VOD. ACACO has been proposed by
Venkate- san et al. for analyzing the behavior of an approximate circuit and
measuring its error metrics such as worst-case error, average-case error, error
probability, and error distribution. Chippa et al. proposed a three-step
framework to reduce the energy usage that includes characterizing the
application for approximation, creating a unique hardware with scaling
mechanisms and using a feedback-control technique at runtime to modulate
dynamic scaling. Nepal et al. created ABACUS, a tool for generating
approximate circuits from arbitrary designs. Kim et al. proposed an
approximate adder that incorporates a parallel carry prediction strategy and an
error correction logic to enhance machine learning (L) applications , and
iguel et al. explored load value approximation for learning value patterns at
a micro architectural level. Esmaeilzadeh et al.  proposed an ISA
extension to support approximate operations and storage. Compared to these
prior studies, their slicing-based strategy is much less intrusive, as the only
hardware support they need is to enable skipping of the data accesses marked by
the slicer.

**Compiler Support**: Paraprox and SAGE by Samadi et al. have been developed for
enhancing the approximate computing capabilities of GPUs [34, 35]. Esmaeilzadeh
et al. investigated a neural network-based approach for accelerating
general-purpose programs. Vassiliadis et al. presented a mathematical model
for determining the significance of code segments and used this model for
determining beneficial regions to approximate. In comparison, ishra et al.
developed iACT toolkit which provides an approximate memorization framework as
well as a runtime and a simulated hardware test bed. Sampson et al.
introduced approximate data types to Java with their extension EnerJ , and
Carbin et al. proposed a code classification technique for identifying critical
regions. Their work is different from these studies as they employ program
slicing to identify the set of data accesses to drop.

# \large  CONCLUSION

First, it investigates the potential benefits of a form of approximate
computing that drops/skips select data accesses in the executions of
multithreaded workloads on merging manycore systems. The unique aspect of this
evaluation is its architecture awareness.  That is, given a bound on program
output error (inaccuracy), they quantify the benefits of dropping the costliest
data accesses (in the target architecture), as opposed to dropping data
accesses randomly. Their experiments with ten different multi-threaded
workloads indicate that being architecture aware in dropping data accesses pays
of, resulting in 27% additional performance improvement (on average) over
randomly dropping the same number of data accesses. Second, it presents a
program slicing-based approach that identifies the set of data accesses to drop
such that (i) the resulting performance/energy benefits are maximized and (ii)
the execution remains within the error (inaccuracy) bound specified by the
user. Their experiments with this approach reveal 8.8% performance improvement
and 13.7% energy saving (on average) are possible when they set the error bound
to 2%, and these improvements increase to 15% and 25%, respectively, when the
error bound is raised to 4%.  

\chapter*{\large REFERENCES}
\addcontentsline{toc}{chapter}{REFERENCES}

[1] Akturk, I., Khatamifard, K., and Karpuzcu, U. R. On quantiication of
accuracy loss in approximate computing.  
[2] Bienia, C., Kumar, S., Singh, J. P., and Li, K. The PARSEC Benchmark Suite:
Characterization and Architectural Implications. In PACT (2008).  
[3] Binkert, N., Beckmann, B., Black, G., Reinhardt, S. K., Saidi, A., Basu,
A., Hestness, J., Hower, D. R., Krishna, T., Sardashti, S., Sen, R., Sewell,
K., Shoaib, M., Vaish, N., Hill, M. D., and Wood, D. A. The gem5 simulator. ACM
SIGARCH Computer Architecture News (2011).  
[4] Carbin, M., and Rinard, M. C. Automatically Identifying Critical Input
Regions and Code in Applications. In Proceedings of the 19th International
Symposium on Software Testing and Analysis.  
[5] Chippa, V. K., Venkataramani, S., Chakradhar, S. T., Roy, K., and
Raghunathan, A. Approximate Computing: An Integrated Hardware Approach. In
Asilomar Conference on Signals, Systems and Computers (2013).  
[6] Ding, W., Tang, X., Kandemir, M., Zhang, Y., and Kultursay, E. Optimizing
Of-chip Accesses in ℧ulticores. In Proceedings of the 36th ACM SIGPLAN
Conference on Programming Language Design and Implementation (PLDI) (2015).
Proc. AC℧ ℧eas. Anal. Comput. Syst., Vol. 3, No. 2, Article 38. Publication
date: June 2019.Architecture-Aware Approximate Computing
38:23  
[7] Esmaeilzadeh, H., Sampson, A., Ceze, L., and Burger, D. Architecture
support for disciplined approximate program- ming. In ASPLOS (2012).  

\chapter*{\large APPENDIX A \break \large PRESENTATION SLIDES}
\pagenumbering{roman}
\addcontentsline{toc}{chapter}{APPENDIX A PRESENTATION SLIDES}

\includegraphics{slide-1}
\newpage
\chapter*{  }
\includegraphics{slide-2}
\newpage
\chapter*{  }
\includegraphics{slide-3}
\newpage
\chapter*{  }
\includegraphics{slide-4}
\newpage
\chapter*{  }
\includegraphics{slide-5}
\newpage
\chapter*{  }
\includegraphics{slide-6}

